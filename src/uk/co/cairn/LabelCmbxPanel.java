package uk.co.cairn;

import mmcorej.StrVector;

import javax.swing.*;

public class LabelCmbxPanel {
    JComboBox comboBox;
    JPanel panel;
    private JLabel label;

    LabelCmbxPanel(String labelText)
    {
        label.setText(labelText);
    }

    void setEnabled(boolean enabled)
    {
        comboBox.setEnabled(enabled);
        label.setEnabled(enabled);
        panel.setEnabled(enabled);
    }

    void setStates(StrVector labels)
    {
        comboBox.removeAllItems();
        for (int i = 0; i < labels.size(); i++)
        {
            comboBox.addItem(labels.get(i));
        }
    }
}
