package uk.co.cairn;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import mmcorej.DeviceType;
import mmcorej.StrVector;
import org.micromanager.Studio;
import org.micromanager.data.*;
import org.micromanager.display.DisplayWindow;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageProducer;
import java.io.*;
import java.util.*;

public class CairnOPTGUI {
    private JTabbedPane tabbedPane1;
    JPanel mainPanel;
    private JComboBox cmbxCamera;
    private JComboBox cmbxRotation;
    private JComboBox cmbxFilterWheel;
    private JSpinner spnrNumFilters;
    private JPanel pnlFilterSettup;
    private JPanel pnlFilters;
    private JButton testButton;
    private JButton btnCalibStep;
    private JButton btnCalibContinuousStep;
    private JButton btnCalibStopStep;
    private JButton btnCalibSpinStart;
    private JSpinner spnCalibCamExposure;
    private JButton btnCalibSpinStop;
    private JButton btnAcquire;
    private JSpinner spnNumberOfSteps;
    private JSpinner spnAcquisitionExposure;
    private JButton btnAbortAcquisition;
    private JRadioButton uniformStepsRadioButton;
    private JRadioButton stepsFromCSVRadioButton;
    private JTextField txtStepCSVLocation;
    private JButton btnStepCSVBrowse;
    private JRadioButton rbReconstructionCurrentImage;
    private JRadioButton rbReconstructionFromFile;
    private JTextField txtReconstructionImageLoc;
    private JButton btnReconImageFileBrowse;
    private JButton btnAcquisitionSave;
    private JRadioButton rbReconstructionUniformSteps;
    private JRadioButton rbReconstructionStepsFromFile;
    private JTextField txtReconstructionStepLoc;
    private JButton btnReconStepFileBrowse;
    private JSpinner spnReconUniformSteps;
    private JTextArea textArea1;
    private JCheckBox chkbxReconstructionNearest;
    private JTextField txtSaveFileName;
    private JButton btnReconstruct;

    private static final int maxNumFilters = 10;

    private LabelCmbxPanel[] filterStates = new LabelCmbxPanel[maxNumFilters];

    private Studio mm;

    private Datastore calibStepDatastore;
    private DisplayWindow calibStepDisplayWindow;
    private Thread calibStepThread;

    private double camExposureTmp;
    private Thread calibSpinThread;

    private Thread acquisitionThread;
    private Datastore currentAcquisitionStore;
    private LinkedList<Double> currentAcquisitionPositions;

    private JFrame frame;

    CairnOPTGUI(Studio studio, JFrame container)
    {
        mm = studio;
        calibStepDatastore = mm.data().createRewritableRAMDatastore();
        frame = container;

        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(2, 2, maxNumFilters, 1);
        spnrNumFilters.setModel(spinnerNumberModel);

        StrVector loadedDevices = mm.core().getLoadedDevices();
        for (int i = 0; i < loadedDevices.size(); i++) {
            try {
                String devName = loadedDevices.get(i);
                DeviceType devType = mm.core().getDeviceType(devName);
                if (devType == DeviceType.CameraDevice)
                    cmbxCamera.addItem(devName);
                else if (devType == DeviceType.StageDevice)
                    cmbxRotation.addItem(devName);
                else if (devType == DeviceType.StateDevice)
                    cmbxFilterWheel.addItem(devName);
            } catch (Exception ex) {
                mm.logs().logError(ex.getMessage());
            }
        }
        cmbxFilterWheel.addItem("None");

        StrVector states = new StrVector();
        if (!cmbxFilterWheel.getSelectedItem().equals("None")) {
            try {
                states = mm.core().getStateLabels((String) cmbxFilterWheel.getSelectedItem());
            } catch (Exception ex) {
                mm.logs().logError(ex.getMessage());
            }
        }

        pnlFilterSettup.setLayout(new BoxLayout(pnlFilterSettup, BoxLayout.Y_AXIS));
        for (int i = 0; i < maxNumFilters; i++) {
            filterStates[i] = new LabelCmbxPanel("Filter " + (i+1) + ":");
            if (i < (Integer) spnrNumFilters.getValue())
                pnlFilterSettup.add(filterStates[i].panel);
            filterStates[i].setStates(states);
        }

        if ((cmbxFilterWheel.getSelectedItem()).equals("None")) {
            pnlFilters.setEnabled(false);
            spnrNumFilters.setEnabled(false);
            for (int i = 0; i < maxNumFilters; i++)
                filterStates[i].setEnabled(false);
        }


        SpinnerNumberModel exposureNumberModel = new SpinnerNumberModel(400.0, 20.0, 1000.0, 1.0);
        spnCalibCamExposure.setModel(exposureNumberModel);

        SpinnerNumberModel acqExposureNumberModel = new SpinnerNumberModel(100.0, 20.0, 1000.0, 1.0);
        spnAcquisitionExposure.setModel(acqExposureNumberModel);

        SpinnerNumberModel acqNumStepsModel = new SpinnerNumberModel(400, 100, 1600, 100);
        spnNumberOfSteps.setModel(acqNumStepsModel);
        spnReconUniformSteps.setModel(acqNumStepsModel);

        cmbxFilterWheel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((cmbxFilterWheel.getSelectedItem()).equals("None")) {
                    pnlFilters.setEnabled(false);
                    spnrNumFilters.setEnabled(false);
                    for (int i = 0; i < maxNumFilters; i++)
                        filterStates[i].setEnabled(false);

                } else {
                    pnlFilters.setEnabled(true);
                    spnrNumFilters.setEnabled(true);

                    StrVector states = new StrVector();
                    try {
                        states = mm.core().getStateLabels((String) cmbxFilterWheel.getSelectedItem());
                    } catch (Exception ex) {
                        mm.logs().logError(ex.getMessage());
                    }

                    for (int i = 0; i < maxNumFilters; i++) {
                        filterStates[i].setEnabled(true);
                        filterStates[i].setStates(states);
                    }
                }
            }
        });

        spnrNumFilters.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                pnlFilterSettup.removeAll();
                for (int i = 0; i < maxNumFilters; i++)
                {
                    if (i < (Integer) spnrNumFilters.getValue())
                        pnlFilterSettup.add(filterStates[i].panel);
                    mainPanel.updateUI();
                }

                frame.pack();
            }
        });

        btnCalibStep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (calibStepDisplayWindow == null || calibStepDisplayWindow.getIsClosed())
                    calibStepDisplayWindow = mm.displays().createDisplay(calibStepDatastore);
                calibStep();
            }
        });

        btnCalibContinuousStep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (calibStepDisplayWindow == null || calibStepDisplayWindow.getIsClosed())
                    calibStepDisplayWindow = mm.displays().createDisplay(calibStepDatastore);


                btnCalibContinuousStep.setEnabled(false);
                btnCalibStep.setEnabled(false);
                btnCalibStopStep.setEnabled(true);
                btnCalibSpinStart.setEnabled(false);

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        while (true)
                        {
                            calibStep();

                            if (Thread.interrupted())
                                return;

                            if (calibStepDisplayWindow.getIsClosed())
                            {
                                btnCalibContinuousStep.setEnabled(true);
                                btnCalibStep.setEnabled(true);
                                btnCalibStopStep.setEnabled(false);
                                return;
                            }

                            try {
                                Thread.sleep(30);
                            } catch (InterruptedException ex) {
                                return;
                            }
                        }
                    }
                };

                calibStepThread = new Thread(r);
                calibStepThread.start();
            }
        });
        btnCalibStopStep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (calibStepThread != null && calibStepThread.isAlive())
                    calibStepThread.interrupt();

                btnCalibContinuousStep.setEnabled(true);
                btnCalibStep.setEnabled(true);
                btnCalibStopStep.setEnabled(false);
                btnCalibSpinStart.setEnabled(true);
            }
        });
        btnCalibSpinStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try
                {
                    mm.core().setCameraDevice((String)cmbxCamera.getSelectedItem());
                    camExposureTmp = mm.core().getExposure();
                } catch (Exception ex) {
                    mm.logs().logError(ex.getMessage());
                    return;
                }

                try
                {
                    StrVector properties = mm.core().getDevicePropertyNames((String)cmbxCamera.getSelectedItem());
                    for (int i = 0; i < properties.size(); i++)
                    {
                        if (properties.get(i).equals("Frame Rate Control Enabled"))
                        {
                            mm.core().setProperty((String)cmbxCamera.getSelectedItem(), "Frame Rate Control Enabled", 0);
                        }

                        if (properties.get(i).equals("Frame Rate Auto"))
                        {
                            mm.core().setProperty((String)cmbxCamera.getSelectedItem(), "Frame Rate Auto", "Off");
                        }
                    }
                } catch (Exception ex) {
                    mm.logs().logMessage(ex.getMessage());
                }


                try
                {
                    mm.core().setExposure((Double)spnCalibCamExposure.getValue());
                } catch (Exception ex)
                {
                    mm.logs().logError(ex.getMessage());
                    return;
                }

                btnCalibSpinStart.setEnabled(false);
                btnCalibSpinStop.setEnabled(true);
                btnCalibStep.setEnabled(false);
                btnCalibContinuousStep.setEnabled(false);
                mm.live().setLiveMode(true);

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        while (true)
                        {
                            if (Thread.interrupted())
                                return;

                            try
                            {
                                mm.core().setPosition((String)cmbxRotation.getSelectedItem(), 360.0);
                            } catch (Exception ex) {
                                mm.logs().logError(ex.getMessage());
                                return;
                            }

                            if (Thread.interrupted())
                                return;

                            try
                            {
                                mm.core().setPosition((String)cmbxRotation.getSelectedItem(), 0.0);
                            } catch (Exception ex) {
                                mm.logs().logError(ex.getMessage());
                                return;
                            }
                        }
                    }
                };

                calibSpinThread = new Thread(r);
                calibSpinThread.start();
            }
        });
        btnCalibSpinStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calibSpinThread.interrupt();

                try
                {
                    Thread.sleep(100);
                } catch (InterruptedException ex) { }

                mm.live().setLiveMode(false);
                btnCalibSpinStart.setEnabled(true);
                btnCalibSpinStop.setEnabled(false);
                btnCalibContinuousStep.setEnabled(true);
                btnCalibStep.setEnabled(true);
                try
                {
                    mm.core().setExposure(camExposureTmp);
                } catch (Exception ex) {
                    mm.logs().logError(ex.getMessage());
                }
            }
        });
        btnAcquire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentAcquisitionStore = mm.data().createRewritableRAMDatastore();
                final DisplayWindow dw = mm.displays().createDisplay(currentAcquisitionStore);


                currentAcquisitionPositions = new LinkedList<Double>();
                if (uniformStepsRadioButton.isSelected()) {
                    double stepSize = 360.0/(Integer)spnNumberOfSteps.getValue();
                    for (int i = 0; i < (Integer) spnNumberOfSteps.getValue(); i++)
                        currentAcquisitionPositions.add(i*stepSize);
                } else {
                    try {

                        InputStreamReader file = new InputStreamReader(new FileInputStream(txtStepCSVLocation.getText()), "UTF-8");
                        Scanner scanner = new Scanner(file);
                        scanner.useDelimiter(",");
                        while (scanner.hasNext()) {
                            currentAcquisitionPositions.add(Double.parseDouble(scanner.next().trim().replace("\uFEFF", "")));
                        }

                    } catch (FileNotFoundException ex) {
                        mm.logs().logError(ex.getMessage());
                        mm.logs().showError(ex.getMessage());
                        return;
                    } catch (UnsupportedEncodingException ex) {
                        mm.logs().logError(ex.getMessage());
                        mm.logs().showError(ex.getMessage());
                    } catch (NumberFormatException ex) {
                        mm.logs().logError(ex.getMessage());
                        mm.logs().showError(ex.getMessage());
                        return;
                    }
                }

                try //disable frame rate control on point grey cameras to allow arbitrary exposures
                {
                    StrVector properties = mm.core().getDevicePropertyNames((String)cmbxCamera.getSelectedItem());
                    for (int i = 0; i < properties.size(); i++)
                    {
                        if (properties.get(i).equals("Frame Rate Control Enabled"))
                        {
                            mm.core().setProperty((String)cmbxCamera.getSelectedItem(), "Frame Rate Control Enabled", 0);
                        }

                        if (properties.get(i).equals("Frame Rate Auto"))
                        {
                            mm.core().setProperty((String)cmbxCamera.getSelectedItem(), "Frame Rate Auto", "Off");
                        }

                        if (properties.get(i).equals("Gain Auto"))
                        {
                            mm.core().setProperty((String)cmbxCamera.getSelectedItem(), "Gain Control", "Off");
                        }
                    }
                } catch (Exception ex) {
                    mm.logs().logMessage(ex.getMessage());
                }

                try
                {
                    mm.core().setCameraDevice((String)cmbxCamera.getSelectedItem());
                    camExposureTmp = mm.core().getExposure();
                    mm.core().setExposure((Double)spnAcquisitionExposure.getValue());
                } catch (Exception ex) {
                    mm.logs().logError(ex.getMessage());
                    mm.logs().showError(ex);
                    return;
                }



                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        String camDev, stageDev, filterWheelDev;

                        btnAcquire.setEnabled(false);
                        btnAbortAcquisition.setEnabled(true);

                        camDev = (String) cmbxCamera.getSelectedItem();
                        stageDev = (String) cmbxRotation.getSelectedItem();
                        filterWheelDev = (String) cmbxFilterWheel.getSelectedItem();
                        boolean useFilterWheel = !filterWheelDev.equals("None");

                        try {
                            mm.core().setCameraDevice(camDev);
                        } catch (Exception ex) {
                            mm.logs().logError(ex.getMessage());
                        }

                        String[] filterNames = new String[maxNumFilters];
                        for (int i = 0; i < maxNumFilters; i++) {
                            filterNames[i] = (String) filterStates[i].comboBox.getSelectedItem();
                        }

                        Coords.CoordsBuilder cb = mm.data().getCoordsBuilder().stagePosition(0).z(0).channel(0).time(0);
                        Iterator<Double> it = currentAcquisitionPositions.iterator();
                        int i = 0;
                        while (it.hasNext())
                        {
                            cb.time(i);
                            try {
                                mm.core().setPosition(stageDev, it.next());
                                Thread.sleep(30);
                            } catch (Exception ex) {
                                mm.logs().logError("Error setting stage position: " + ex.getMessage());
                            }

                            boolean interrupted = false;
                            if (useFilterWheel)
                            {
                                for (int j = 0; j < (Integer) spnrNumFilters.getValue(); j++)
                                {
                                    cb.channel(j);
                                    try {
                                        mm.core().setStateLabel(filterWheelDev, filterNames[j]);
                                        Thread.sleep(30);
                                    } catch (InterruptedException ex) {
                                        interrupted = true;
                                    } catch (Exception ex) {
                                        mm.logs().logError("Error setting filter wheel position: " + ex.getMessage());
                                    }


                                    Image image = mm.live().snap(false).get(0);
                                    image = image.copyAtCoords(cb.build());

                                    try {
                                        currentAcquisitionStore.putImage(image);
                                    } catch (DatastoreFrozenException ex) {
                                        mm.logs().logError("Datastore frozen error storing image: " + ex.getMessage());
                                    } catch (DatastoreRewriteException ex) {
                                        mm.logs().logError("Datastore rewrite error storing image: " + ex.getMessage());
                                    }
                                }
                            } else {
                                Image image = mm.live().snap(false).get(0);
                                image = image.copyAtCoords(cb.build());

                                try {
                                    currentAcquisitionStore.putImage(image);
                                } catch (DatastoreFrozenException ex) {
                                    mm.logs().logError("Datastore frozen error storing image: " + ex.getMessage());
                                } catch (DatastoreRewriteException ex) {
                                    mm.logs().logError("Datastore rewrite error storing image: " + ex.getMessage());
                                }
                            }

                            if (Thread.interrupted() || interrupted)
                            {
                                try
                                {
                                    mm.core().setExposure(camExposureTmp);
                                } catch (Exception ex) {
                                    mm.logs().logError(ex);
                                }

                                btnAbortAcquisition.setEnabled(false);
                                btnAcquire.setEnabled(true);
                                btnAcquisitionSave.setEnabled(true);
                                return;
                            }

                            i++;
                        }

                        try
                        {
                            mm.core().setExposure(camExposureTmp);
                        } catch (Exception ex) {
                            mm.logs().logError(ex);
                        }

                        btnAbortAcquisition.setEnabled(false);
                        btnAcquire.setEnabled(true);
                        btnAcquisitionSave.setEnabled(true);
                    }
                };

                acquisitionThread = new Thread(r);
                acquisitionThread.start();
            }
        });
        btnAbortAcquisition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                acquisitionThread.interrupt();
            }
        });
        stepsFromCSVRadioButton.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean csv = stepsFromCSVRadioButton.isSelected();
                spnNumberOfSteps.setEnabled(!csv);
                btnStepCSVBrowse.setEnabled(csv);
                txtStepCSVLocation.setEnabled(csv);

                rbReconstructionStepsFromFile.setSelected(csv);
                rbReconstructionUniformSteps.setSelected(!csv);
            }
        });
        btnStepCSVBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               handleCSVLoadDialog();
            }
        });
        btnAcquisitionSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               JFileChooser jfc = new JFileChooser();
               jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                if (jfc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
                   return;


                Coords maxCoords = currentAcquisitionStore.getMaxIndices();
                Coords.CoordsBuilder cb = mm.data().getCoordsBuilder().time(0).channel(0).stagePosition(0).z(0);

                for (int t = 0; t <= maxCoords.getTime(); t++)
                {
                    Formatter formatter = new Formatter();
                    String imageName;
                    if (maxCoords.getTime() >= 1000)
                        imageName = txtSaveFileName.getText().replace(".tif", "") + formatter.format("%04d", t);
                    else
                        imageName = txtSaveFileName.getText().replace(".tif", "") + formatter.format("%03d", t);

                    String filename = jfc.getSelectedFile().getAbsolutePath() +
                            "/" +
                            imageName +
                            ".tif";

                    cb.time(t);
                    Image im = currentAcquisitionStore.getImage(cb.build());
                    IJ.saveAsTiff(new ImagePlus(imageName, mm.data().ij().createProcessor(im)), filename);
                }


            }
        });
        btnReconImageFileBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser();
                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                int r = jfc.showOpenDialog(null);
                if (r != JFileChooser.APPROVE_OPTION)
                    return;

                txtReconstructionImageLoc.setText(jfc.getSelectedFile().getAbsolutePath());
            }
        });
        rbReconstructionFromFile.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                txtReconstructionImageLoc.setEnabled(rbReconstructionFromFile.isSelected());
                btnReconImageFileBrowse.setEnabled(rbReconstructionFromFile.isSelected());
            }
        });
        rbReconstructionStepsFromFile.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                boolean csv = rbReconstructionStepsFromFile.isSelected();
                txtReconstructionStepLoc.setEnabled(csv);
                btnReconStepFileBrowse.setEnabled(csv);

                stepsFromCSVRadioButton.setSelected(csv);
                uniformStepsRadioButton.setSelected(!csv);
            }
        });
        btnReconStepFileBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                handleCSVLoadDialog();
            }
        });
        btnReconstruct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LinkedList<Double> positions = new LinkedList<Double>();
                if (rbReconstructionUniformSteps.isSelected()) {
                    double dTheta = 360.0/(Integer)spnNumberOfSteps.getValue();
                    for (int i = 0; i < (Integer)spnNumberOfSteps.getValue(); i++) {
                        positions.add(i*dTheta);
                    }
                }

                Runnable r = new Runnable() {
                    @Override
                    public void run() {

                        Reconstructor r = new Reconstructor(mm, txtReconstructionImageLoc.getText(), null);
                        if (!r.initialise()) {
                            mm.logs().showError("Error initialising reconstruction routines");
                            return;
                        }

                        if (!r.reconstruct()) {
                            mm.logs().showError("Error performing reconstruction!");
                        }
                    }
                };

                new Thread(r).start();
            }
        });
    }

    private void calibStep() {
        String stageDev = (String) cmbxRotation.getSelectedItem();

        double currentStagePosition;
        try {
            currentStagePosition = mm.core().getPosition(stageDev);
            Coords.CoordsBuilder cb = mm.data().getCoordsBuilder().time(0).channel(0);

            Image im1 = mm.live().snap(false).get(0);
            im1 = im1.copyAtCoords(cb.build());

            mm.core().setPosition(stageDev, currentStagePosition + 180);
            cb.channel(1);
            Image im2 = mm.live().snap(false).get(0);

            mm.core().setPosition(stageDev, currentStagePosition);

            ImageProcessor ip = mm.data().ij().createProcessor(im2);
            //ip.flipHorizontal();
            ip.flipVertical();
            im2 = mm.data().ij().createImage(ip, cb.build(), im2.getMetadata());

            calibStepDatastore.putImage(im1);
            calibStepDatastore.putImage(im2);
        } catch (DatastoreFrozenException ex) {
            mm.logs().logError(ex.getMessage());
        } catch (DatastoreRewriteException ex) {
            mm.logs().logError(ex.getMessage());
        } catch (Exception ex) {
            mm.logs().logError(ex.getMessage());
        }
    }

    private void handleCSVLoadDialog() {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV Files", "csv");
        final JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(filter);

        int r = jfc.showOpenDialog(null);

        if (r != JFileChooser.APPROVE_OPTION)
            return;

        txtStepCSVLocation.setText(jfc.getSelectedFile().getAbsolutePath());
        txtReconstructionStepLoc.setText(jfc.getSelectedFile().getAbsolutePath());
    }
}
