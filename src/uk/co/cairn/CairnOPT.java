package uk.co.cairn;

import mmcorej.StrVector;
import org.micromanager.MenuPlugin;
import org.micromanager.Studio;
import org.scijava.plugin.Plugin;
import org.scijava.plugin.SciJavaPlugin;

import javax.swing.*;

@Plugin(type = MenuPlugin.class)
public class CairnOPT implements MenuPlugin, SciJavaPlugin {

    @Override
    public String getSubMenu() {
        return "Cairn";
    }

    @Override
    public void onPluginSelected() {
        JFrame frame = new JFrame();
        CairnOPTGUI gui = new CairnOPTGUI(mm, frame);
        frame.setTitle("Cairn OPT");
        frame.setContentPane(gui.mainPanel);
        frame.pack();
        //frame.setSize(800, 600);
        frame.setVisible(true);
    }

    @Override
    public void setContext(Studio studio) {
        mm = studio;
    }

    @Override
    public String getName() {
        return "Cairn OPT";
    }

    @Override
    public String getHelpText() {
        return "Nope";
    }

    @Override
    public String getVersion() {
        return "0.0.0.0.0";
    }

    @Override
    public String getCopyright() {
        return "Me";
    }

    private Studio mm;
}
