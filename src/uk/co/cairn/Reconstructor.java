package uk.co.cairn;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ByteProcessor;
import ij.process.FHT;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import org.micromanager.Studio;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.LinkedList;

import uk.co.cairn.CuOPT.CuOPT_JNI;

public class Reconstructor {
    ImageStack stack;
    String fileLoc;
    LinkedList<Double> pos;
    Studio mm;


    Reconstructor(Studio studio, String fileLocation, LinkedList<Double> positions) {
        mm = studio;
        fileLoc = fileLocation;
        pos = positions;
    }

    Reconstructor(Studio studio, LinkedList<Double> positions) {
        mm = studio;
        fileLoc = null;
        pos = positions;
    }

    boolean initialise() {
        if (fileLoc != null)
        {
            File dir = new File(fileLoc);
            if (!dir.isDirectory())
                return false;

            String[] files = dir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".tif");
                }
            });

            if (files == null)
                return false;

            IJ.showStatus("Opening Images");
            stack = null;
            for (String file : files) {
                ImageProcessor ip = IJ.openImage(fileLoc + "/" + file).getProcessor();
                if (stack == null)
                    stack = new ImageStack(ip.getWidth(), ip.getHeight());

                FloatProcessor fp = ip.convertToFloatProcessor();
                if (ip.getBitDepth() == 8)
                    fp.multiply(1/255.0);
                else if (ip.getBitDepth() == 16)
                    fp.multiply(1/65535.0);
                stack.addSlice(fp);

                IJ.showProgress(stack.getSize(), files.length);
            }

        }
        return true;
    }

    boolean reconstruct() {
        new ImagePlus("Input Image", stack).show();

        CuOPT_JNI cuOPT = new CuOPT_JNI();

        final int numSlices = Math.min(cuOPT.GetMaxSlices(stack.getWidth(), stack.getSize()), stack.getHeight());


		CuOPT_JNI.InitData id = new CuOPT_JNI.InitData();
		id.width = stack.getWidth();
		id.height = numSlices;
		id.nTheta = stack.getSize();
		id.outWidth = stack.getWidth();
		id.outHeight = stack.getWidth();

        ByteBuffer buf = ByteBuffer.allocateDirect(id.outHeight*id.outWidth*stack.getHeight()*4);
		id.outBuffer = buf;

		cuOPT.RegisterCallback(new CuOPT_JNI.OPT_Callback() {
            @Override
            public void callback(int i) {
                IJ.showProgress(i, numSlices);
            }
        });

		IJ.showMessage("Reconstructing...");
		if (!cuOPT.Initialize(id))
		{
			String err = cuOPT.GetLastError();

			IJ.showMessage(err);
			return false;
		}
        boolean failed = false;

		for (int offset = 0; offset < stack.getHeight(); offset += numSlices)
        {
            int nSlices = Math.min(stack.getHeight() - offset, numSlices);
            for (int i = 0; i < stack.getSize(); i++)
            {
                if (!cuOPT.AddImage((float[])stack.getPixels(i+1), offset, nSlices)) {
                    String err = cuOPT.GetLastError();
                    IJ.showMessage(err);

                    failed = true;
                    break;
                }
            }

            if (!cuOPT.Reconstruct(nSlices))
            {
                String err = cuOPT.GetLastError();
                IJ.showMessage(err);

                failed = true;
                break;
            }

            if (failed)
                break;
        }

		if (!cuOPT.Deinitialize())
		{
			String err = cuOPT.GetLastError();

            IJ.showMessage(err);
			return false;
		}

		ImageStack out = new ImageStack(id.outWidth, id.outHeight);
		if (failed) {
            IJ.showMessage("Reconstruction Failed!");
        } else {
		    final ByteBuffer slice = buf.slice();
		    //double max = Double.NEGATIVE_INFINITY, min = Double.POSITIVE_INFINITY;

		    for (int i = 0; i < stack.getHeight(); i++)
            {
                slice.position(i*id.outWidth*id.outHeight*4);
                slice.limit((i+1)*id.outWidth*id.outHeight*4);

                FloatBuffer fb = slice.order(ByteOrder.nativeOrder()).asFloatBuffer();
                float[] imgArr = new float[fb.limit()];

                fb.get(imgArr);
                //for (int j = 0; j < imgArr.length; j++)
                    //imgArr[j] = fb.get();

                FloatProcessor fp = new FloatProcessor(id.outWidth, id.outHeight, imgArr);
                out.addSlice(fp);

                /*for (int j = 0; j < imgArr.length; j++)
                {
                    max = Math.max(max, imgArr[j]);
                    min = Math.min(min, imgArr[j]);
                }*/
            }

            /*for (int i = 1; i <= stack.getHeight(); i++)
            {
                out.getProcessor(i).subtract(min);
                out.getProcessor(i).multiply(1.0/(max-min));
            }*/
        }

        ImagePlus outImPlus = new ImagePlus("Output", out);
		outImPlus.resetDisplayRange();
		outImPlus.show();

		IJ.showMessage("Success");
        return true;
    }

}
